package Pages;

import static Base.Waits.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MarketPage
{

    WebDriver driver;
    public static String staticProduct;
    public MarketPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void goToSubSection(String name)
    {
        Click(driver.findElement(By.xpath("//a[contains(@class, 'topmenu__link') and text()='"+name+"']")),10);
    }

    public void goToCatalog(String name) throws InterruptedException
    {
        Click(driver.findElement(By.xpath("//div[contains(@class, 'catalog-menu_js_inited')]//a[text()='"+name+"']")),10);
    }

    public void goToFilters()
    {
        Click(allFilters, 10);
    }

    public void clickOnApply(String name)
    {
        Click(driver.findElement(By.xpath("//span[text() = '"+name+"']//parent::a")), 10);
    }

    public void doFilterByFirm(List<String> firm) throws InterruptedException
    {
        Click(viewAll, 10);
        waitUntilElementVisible(search, 10);
        for (int i = 0; i < firm.size(); i++)
        {
            search.clear();
            search.sendKeys(firm.get(i));
            Thread.sleep(1000);
            ClickByLoc(By.xpath("//div[@data-filter-id = '7893318']//label[text()='"+firm.get(i)+"']//parent::span"), 10);
        }
    }

    public void doFilterByPrice(String from, String to)
    {
        if (!from.equalsIgnoreCase(""))
        {
            waitUntilElementVisible(priceFrom, 10);
            priceFrom.sendKeys(from);
        }
        if (!to.equalsIgnoreCase(""))
        {
            waitUntilElementVisible(priceTo, 10);
            priceTo.sendKeys(to);
        }
    }

    public void rememberProduct(int count)
    {
        staticProduct = productsName.get(count - 1).getText();
    }

    public void searchProduct()
    {
        waitUntilElementVisible(headerSearch, 10);
        headerSearch.sendKeys(staticProduct);
        Click(searchButton, 10);
    }


    // Показать всё в фильтре по производителю
    @FindBy(xpath = ("//div[@data-filter-id = '7893318']//button"))
    public WebElement viewAll;

    // Поле поиска в фильтре по производителю
    @FindBy(xpath = ("//div[@data-filter-id = '7893318']//input[@class = 'input__control']"))
    public WebElement search;

    // Цена ОТ
    @FindBy(xpath = ("//input[contains(@id, 'glf-pricefrom-var')]"))
    public WebElement priceFrom;

    // Цена ДО
    @FindBy(xpath = ("//input[contains(@id, 'glf-priceto-var')]"))
    public WebElement priceTo;

    // Название товара в Маркете
    @FindAll(@FindBy(xpath = "//div[contains(@class, 'n-snippet-card2__title')]"))
    public List<WebElement> productsName;

    // Кнопка Перейти ко всем фильтрам
    @FindBy(xpath = "//a[text() = 'Перейти ко всем фильтрам'] | //span[text() = 'Все фильтры']//parent::button")
    public WebElement allFilters;

    // Поиск вверху страницы
    @FindBy(xpath = "//input[@id = 'header-search']")
    public WebElement headerSearch;

    // Кнопка Найти
    @FindBy(xpath = "//span[text() = 'Найти']//parent::button")
    public WebElement searchButton;
}
