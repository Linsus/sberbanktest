package Hooks;

import Base.Base;
import cucumber.api.Scenario;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks
 {
    @Before
    public void beforeScenario()
    {
        System.setProperty("webdriver.chrome.driver", "./webdrivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "./webdrivers/geckodriver.exe");
    }

    @After
    public void afterScenario(Scenario scenario) throws IOException, AWTException
    {
        if (scenario.isFailed())
        {
            BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd_mm_yyyy hh_mm_ss ");
            ImageIO.write(image, "png", new File("./Screenshoots/" + formatForDateNow.format(dateNow) + scenario.getName() + ".png"));
        }
        Base.driver.quit();
    }
}
