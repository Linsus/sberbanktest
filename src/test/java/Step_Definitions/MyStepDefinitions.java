package Step_Definitions;

import Base.Base;
import Pages.MainPage;
import Pages.MarketPage;
import Pages.ProductPage;
import cucumber.api.java.ru.Допустим;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.List;

public class MyStepDefinitions
{
    MainPage main;
    MarketPage market;
    ProductPage product;

    @Допустим("^Открыли \"([^\"]*)\"$")
    public void открылиБраузер(String browser) throws Throwable
    {
        if (browser.equalsIgnoreCase("Chrome"))
            Base.driver = new ChromeDriver();
        else if (browser.equalsIgnoreCase("Firefox"))
            Base.driver = new FirefoxDriver();
        Base.driver.manage().window().maximize();
        main = new MainPage(Base.driver);
        market = new MarketPage(Base.driver);
        product = new ProductPage(Base.driver);
    }

    @Когда("^зашли на страницу \"([^\"]*)\"$")
    public void зашлиНаСтраницу(String page) throws Throwable
    {
        Base.driver.navigate().to(page);
    }

    @И("^перешли в \"([^\"]*)\"$")
    public void перешлиВ(String section) throws Throwable
    {
        main.goToSection(section);
    }

    @И("^выбрали подраздел \"([^\"]*)\"$")
    public void выбралиПодраздел(String subsection) throws Throwable
    {
        market.goToSubSection(subsection);
    }

    @И("^выбрали каталог \"([^\"]*)\"$")
    public void выбралиКаталог(String name) throws Throwable
    {
        market.goToCatalog(name);
    }

    @И("^зашли в расширенный поиск$")
    public void зашлиВРасширенныйПоиск() throws Throwable
    {
        market.goToFilters();
    }

    @И("^задали параметр по цене от \"([^\"]*)\" до \"([^\"]*)\"$")
    public void задалиПараметрПоЦенеОтДо(String from, String to) throws Throwable
    {
        market.doFilterByPrice(from, to);
    }

    @И("^выбрали производителя \"([^\"]*)\"$")
    public void выбралиПроизводителя(List<String> firm) throws Throwable
    {
        market.doFilterByFirm(firm);
    }

    @И("^нажали кнопку \"([^\"]*)\"$")
    public void нажалиКнопку(String name) throws Throwable
    {
        market.clickOnApply(name);
    }

    @Тогда("^проверили, что элементов на странице \"([^\"]*)\"$")
    public void проверилиЧтоЭлементовНаСтранице(int count) throws Throwable
    {
        Assert.assertEquals("Количество элементов на странице не равно "+count, count, market.productsName.size());
    }

    @Когда("^запомнили \"([^\"]*)\" элемент в списке$")
    public void запомнилиЭлементВСписке(int count) throws Throwable
    {
        market.rememberProduct(count);
    }

    @Тогда("^нашли и проверили, что наименование товара соответствует запомненному значению$")
    public void нашлиИПроверилиЧтоНаименованиеТовараСоответствуетЗапомненномуЗначению() throws Throwable
    {
    }

    @И("^посчитали количество доступных торговых площадок в разделе «Предложения Магазинов»$")
    public void посчиталиКоличествоДоступныхТорговыхПлощадокВРазделеПредложенияМагазинов() throws Throwable
    {
    }

    @И("^в поисковую строку ввели запомненное значение$")
    public void вПоисковуюСтрокуВвелиЗапомненноеЗначение() throws Throwable
    {
        market.searchProduct();
        product.checkRememberedProduct();
    }

    @И("^вывели количество доступных торговых площадок в консоль$")
    public void вывелиКоличествоДоступныхТорговыхПлощадокВКонсоль() throws Throwable
    {
        product.countAndPrint();
    }
}
