package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waits {

    public static void waitUntilElementVisible(WebElement element, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(Base.driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void Click(WebElement element, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(Base.driver,timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public static void ClickByLoc(By loc, int timeout)
    {
        WebDriverWait wait = new WebDriverWait(Base.driver,timeout);
        wait.until(ExpectedConditions.elementToBeClickable(loc));
        Base.driver.findElement(loc).click();
    }
}
