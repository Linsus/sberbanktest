package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class MainPage
{
    WebDriver driver;

    public MainPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void goToSection(String name)
    {
        driver.findElement(By.xpath("//div[@class='home-arrow__tabs']//a[text()='"+name+"']")).click();
    }
}
