package Pages;

import static Base.Waits.*;

import Base.Base;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ProductPage
{
    WebDriver driver;
    public ProductPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void checkRememberedProduct()
    {
        MarketPage market = new MarketPage(Base.driver);
        try
        {
            for (int i = 0; i < 10; i++)
                if (recovery.isDisplayed())
                {
                    Click(market.searchButton,10);
                }
        }
        catch (Exception e)
        {}
        waitUntilElementVisible(productName, 10);
        Assert.assertTrue("Название продукта не совпадает с тем, которое мы запомнили", productName.getText().contains(MarketPage.staticProduct));
    }

    public void countAndPrint()
    {
        waitUntilElementVisible(topOffers.get(0), 10);
        System.out.println("Количество предложений магазинов: "+topOffers.size());
        System.out.println("");
    }

    // Название товара на странице товара в Маркете
    @FindBy(xpath = "//h1[contains(@class, 'itle_size_28')]")
    public WebElement productName;

    // Recovery trigger
    @FindBy(xpath = "//div[contains(text(), 'в одной категории')]")
    public WebElement recovery;

    // Предложения магазинов
    @FindAll(@FindBy(xpath = "//div[contains(@class, 'n-product-top-offer__item_type_shop')]"))
    public List<WebElement> topOffers;

}
